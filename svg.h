#ifndef SVG_H
#define SVG_H

#include "list"
#include "QDebug"
#include "QFile"
#include "QXmlStreamReader"
#include "QRegularExpression"

class Path;

class SVG {
public:
    SVG(QString filename);
protected:
    int width;
    int height;
    std::list<std::shared_ptr<Path>> paths;
private:
    class Path{
    public:
        void addPoint(QPoint p);
        void setStrokeWidth(float w);
        void setStrokeColor(QString c);
        void setFillColor(QString c);
    private:
        std::list<std::shared_ptr<QPoint>> points;
        float strokeWidth;
        QString strokeColor;
        QString fillColor;
    };
};

#endif // SVG_H
