#include "canvas.h"

Canvas::Canvas(QWidget *parent) : QWidget(parent){
    QPalette pal = palette();
    pal.setColor(QPalette::Background, Qt::white);
    this->setPalette(pal);
}

void Canvas::paintEvent(QPaintEvent *e){
    QPainter p(this);
    for (int i = 25; i < this->width(); i += 25) {
        for (int j = 25; j< this->height(); j += 25) {
            p.drawPoint(i, j);
        }
    }
}

void Canvas::showCanvasMenu(const QPoint &pos){
    QMenu *menu = new QMenu(this);

    QAction *addItem = new QAction("Add item", this);
    connect(addItem, &QAction::triggered, this, &Canvas::addItemToCanvasMenuHandler);
    QAction *deleteItem = new QAction("Delete item", this);
    connect(deleteItem, &QAction::triggered, this, &Canvas::deleteItemFromCanvasMenuHandler);

    menu->addAction(addItem);
    menu->addAction(deleteItem);

    menu->popup(this->mapToGlobal(pos));
}

void Canvas::addItemToCanvasMenuHandler(){
    QString filename = QFileDialog::getOpenFileName(this,
        tr("Add SVG file to project"), QDir::homePath(), tr("Image Files (*.svg)"));
    if(filename == "") return;

    std::shared_ptr<SVG> svg;
    try {
         svg = std::make_shared<SVG>(filename);
    } catch (QXmlParseException e) {
        qWarning() << e.message();
    }
}

void Canvas::deleteItemFromCanvasMenuHandler(){
    qDebug() << "Deleting item";
}
