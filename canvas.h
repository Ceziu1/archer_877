#ifndef CANVAS_H
#define CANVAS_H

#include "QDialog"
#include "QDebug"
#include "QMenu"
#include "QPainter"
#include "QFileDialog"
#include "QXmlParseException"
#include "list"
#include "svg.h"

class Canvas : public QWidget
{
    Q_OBJECT
public:
    explicit Canvas(QWidget *parent = nullptr);

protected:
    void paintEvent(QPaintEvent *e);
    std::list<std::shared_ptr<SVG>> drawableObjects;

private:
    //canvas context menu handlers
    void addItemToCanvasMenuHandler();
    void deleteItemFromCanvasMenuHandler();

signals:
    void resized();

private slots:
    void showCanvasMenu(const QPoint &pos);
};


#endif // CANVAS_H
