#include "svg.h"

SVG::SVG(QString filename) {
    QFile file(filename);
    if (!file.open(QFile::ReadOnly)){
        qWarning() << "Can not open file: " << filename;
        return;
    }
    QXmlStreamReader xml(&file);

    if (xml.readNextStartElement()) { // 'svg' tag
        this->width = xml.attributes().value("width").toInt();
        this->height = xml.attributes().value("height").toInt();
    }

    xml.readNextStartElement();
    xml.readNextStartElement(); // double 'g' tag

    QRegularExpression re("(\\d+\\.\\d+)\\,\\W(\\d+\\.\\d+)");
    while (xml.readNextStartElement()) {
        QRegularExpressionMatchIterator match = re.globalMatch(xml.attributes().value("d"));
        while(match.hasNext()){
            qDebug() << match.next().captured(0);
        }
        xml.skipCurrentElement();
    }
}
