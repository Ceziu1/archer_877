#include "archerwindow.h"
#include "ui_archerwindow.h"

ArcherWindow::ArcherWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ArcherWindow)
{
    ui->setupUi(this);
}

ArcherWindow::~ArcherWindow()
{
    delete ui;
}
