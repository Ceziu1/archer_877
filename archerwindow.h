#ifndef ARCHERWINDOW_H
#define ARCHERWINDOW_H

#include <QMainWindow>

namespace Ui {
class ArcherWindow;
}

class ArcherWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit ArcherWindow(QWidget *parent = nullptr);
    ~ArcherWindow();

private slots:

private:
    Ui::ArcherWindow *ui;
};

#endif // ARCHERWINDOW_H
